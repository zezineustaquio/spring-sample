# Projeto Spring Boot
> Este projeto deve ser utilizado de referência para o desenvolviemnto de aplicações Java utilizando o Spring Framework em aplicações auto contidas.

## Atenção
#### Qualquer melhoria, correção ou adição de exemplos pode ser desenvolvida e solicitado um Merge Request para aprovação.

## Sumário

* [Criacao de um novo projeto](README.md#criacao-de-um-novo-projeto)
    * [Configuracao de ambiente](README.md#configuracao-de-ambiente)
        * [Nexus](README.md#nexus)
        * [Criacao do projeto](README.md#criacao-do-projeto)
* [Estrutura do projeto](README.md#estrutura-de-pacotes-do-projeto)
* [Configuracao da aplicacao](README.md#configuracao-da-aplicacao)
* [Criacao dos profiles](README.md#criacao-dos-profiles)
* [Arquivos de properties](README.md#arquivos-de-properties)
* [Referenciar properties](README.md#referenciar-properties)
* [Swagger-UI](README.md#swagger-ui)
* [Eureka](README.md#eureka)
* [Lombok](README.md#lombok)
* [Health Check](README.md#health-check)
    * [Estrutura da exposicao REST](README.md#estrutura-da-exposicao-rest)
    * [Implementacao](README.md#implementacao)
* [Tratamento de excecoes](README.md#tratamento-de-excecoes)
* [Logging](README.md#logging)
    * [Implementacao](README.md#implementacao-1)
    * [Configuracao](README.md#configuracao)
* [Testes unitarios](README.md#testes-unitarios)
* [Acesso a base de dados](README.md#acesso-a-base-de-dados)
    * [Data Source](README.md#data-source)
    * [Pool de Conexao](README.md#pool-de-conexao)
    * [Mapeando entidades](README.md#mapeando-entidades)
    * [Queries nativas](README.md#queries-nativas)
    * [Stored Procedures](README.md#stored-procedures)

## Criacao de um novo projeto
### Configuracao de ambiente
Utilizaremos neste documento o [Eclipse](https://www.eclipse.org/downloads/) como IDE de desenvolvimento da aplicação.  
#### Nexus
Primeiramente devemos realizar a configuração do [Nexus](http://nexus.tribanco.com.br:8081/) que é o repositório padrão do Tribanco.  
Realize a criação ou edição do arquivo de configuração do Maven `settings.xml` que deve ficar na pasta do usuário da máquina em `C:\Users\{usuario}\.m2\settings.xml`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
	<mirrors>
		<mirror>
			<id>nexus-mirror</id>
			<mirrorOf>*</mirrorOf>
			<name>Mirror Nexus Tribanco</name>
			<url>http://nexus.tribanco.com.br:8081/repository/maven-public</url>
		</mirror>
	</mirrors>
	<profiles>
		<profile>
			<id>nexus</id>
			<repositories>
				<repository>
					<id>central</id>
					<name>Repositorio Tribanco</name>
					<url>http://nexus.tribanco.com.br:8081/repository/maven-public</url>
					<releases>
						<enabled>true</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
			</repositories>
			<pluginRepositories>
				<pluginRepository>
					<id>central</id>
					<url>http://nexus.tribanco.com.br:8081/repository/maven-public</url>
					<releases>
						<enabled>true</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</pluginRepository>
			</pluginRepositories>
		</profile>
	</profiles>
	<activeProfiles>
		<activeProfile>nexus</activeProfile>
	</activeProfiles>
</settings>
```
#### Criacao do projeto
Para realizar a criação de um projeto utilizando a estrutura descrita neste documento, está disponível no Nexus um `archetype`. Assim na criação de um novo `Maven Project` poderá ser adicionado o archetype `spring-business-archetype`.
![New Project](doc/new-maven-project.png)

![Maven Archetype](doc/new-maven-project-archetype-config.png)

![Add Archetype](doc/new-maven-project-archetype-add.png)

![Select Archetype](doc/new-maven-project-archetype.png)  

Após a seleção do Archetype, deverão ser preenchidos os valores de acordo com a aplicação que será desenvolvida como o exemplo abaixo:

![Preencher Archetype](doc/new-maven-project-archetype-fill.png)

* **Group Id:** `groupId` do pom aplicação
* **Artifact Id:** `artifactId` do pom da aplicação
* **Version:** `version` do pom da aplicação
* **Package:** Pacote base da aplicação
* **Name:** Nome da aplicação utilizado no Swagger-UI
* **ArtifactName:** Nome do artefato que será gerado pela aplicação
* **Description:** Breve descrição da função desempenhada pela aplicação. Também é exibido no Swagger-UI

## Padrão de código

Para que o código tenha uma identidade única e tenha uma formatação uniforme independentemente de quem o desenvolveu, utilizamos um formatador customizado, que pode ser facilmente incluído no Eclipse. Diferentemente do formatador padrão do Eclipse, esse formatador utiliza espaços ao invés de tabs, o que garante a consistência de indentação em qualquer ambiente onde o código for visualizado. Além disso, esse formatador também quebra linhas em no máximo 100 caracteres, o que facilita a visualização de código em:

* Telas pequenas
* Telas divididas
* Merge / pull requests no GitLab

### Qual a importância de manter o código padronizado?

É interessante que o código das aplicações siga o mesmo padrão de formatação para evitar discussões sobre estilo de codificação durante o desenvolvimento / revisão de código, e para também evidenciar mudanças em diffs. É importante também que esse padrão possua uma "[opinião](https://www.thoughtworks.com/radar/techniques/opinionated-and-automated-code-formatting)", justamente para garantir que há apenas uma maneira correta.

Caso a equipe opte por escolher outro padrão, documente isso no README.md da aplicação e forneça o formatador utilizado no repositório, permitindo com que outros desenvolvedores atuem no projeto e não quebrem a formatação.

### Importando o formatador

No Eclipse, vá em Window > Preferences. Na caixa de pesquisa, entre com "Formatter" (sem aspas).

![Menu formatter](doc/menu-formatter.png)

Clique no botão import.

![Importar formatter](doc/importar-formatter.png)

Isso abrirá uma caixa de diálogo para selecionar um arquivo. Selecione o arquivo "TribancoFormatter.xml", disponível na raíz do projeto, e dê apply.

![Aplicar formatter](doc/aplicar-formatter.png)

### Save Actions

Pode ser interessante configurar o Eclipse para realizar ações automaticamente após o arquivo ser salvo (formatar, por exemplo). Isso garante que todo código commitado está na formatação adequada, além de fazer algumas tarefas repetitivas, mas que são importantes, como por exemplo:

* Incluir chaves em blocos que possuem apenas uma linha
* Converter loops for para forEach
* Adicionar @Override em métodos sobrescritos
* Organizar imports
* Adicionar final onde possível

Para configurar as save actions, na mesma tela de Preferences, pesquise por "Save Actions", e selecione a opção.

![Buscar save actions](doc/save_actions_1.png)

Habilite o checkbox "Perform the selected actions on save". Com isso, o Eclipse liberará a escolha de quais ações realizar quando o código for salvo. Habilite as opções:
* "Format source code"
* Escolha a opção "Format all lines" (a opção "Format edited lines" pode causar inconsistências na formatação)
* "Organize imports" (utilizamos o default do Eclipse)
* Additional actions

Em additional actions, clique em "Configure..." e marque as opções mostradas nas imagens abaixo.

![Save actions adicionais](doc/additional_save_actions_1.png)

![Save actions adicionais](doc/additional_save_actions_2.png)

![Save actions adicionais](doc/additional_save_actions_3.png)

![Save actions adicionais](doc/additional_save_actions_4.png)

Caso deseje, habilite outros Save Actions (e documente-os no README.md do projeto). Cuidado com os save actions da seção "Unused code" (na aba "Unnecessary code"), que podem excluir código que você acabou de desenvolver.

## Estrutura de pacotes do projeto
![Estrutura de pacotes](doc/estrutura-pacotes.png)
* **br.com.arquitetura.sample.spring:** Pacote base da aplicação
    * **config**
        * **datasource:** Classes de configuração dos Data Sources da aplicação
        * **documentation:** Classe de configuração do Swagger
    * **controller:** Camada de exposição de serviços REST. Pode realizar chamadas apenas para as classes da camada `service`.
    * **service:** Camada de regras de negócio da aplicação. Pode realizar chamadas para outras classes da camada `service` e classes da camada `repository`.
    * **db:** Camada de acesso a banco de dados  
        * **{tipo}.{instancia}.entity:** Classes de mapeamento das entidades de banco de dados
        * **{tipo}.{instancia}.repository:** Classes dos repositórios de acesso a dados. Deve ser chamado apenas da camada `service`.  
**`{tipo}:`** sql, oracle, Sybase  
**`{instancia}:`** Nome da instancia de acordo com a planilha **[Instancias DB.xlsx](doc/instancias-db.xlsx)**
    * **external:** Camada de acesso a serviços externos, chamadas REST, SOAP ou outras integrações. Deve ser chamado apenas da camada `service`.

## Configuracao da aplicacao
A classe `Application.java` deve ser alterada para incluir o pacote padrão da aplicação na anotação `@ComponentScan`.  
As demais configurações de **Locale** e **TimeZone** já estão realizadas conforme solução de problemas passados em eventos como troca do horário de verão.  
A configuração do [Eureka](README.md#eureka) com a anotação **@EnableDiscoveryClient** é necessário para que a aplicação consiga se registrar no servidor para manter o controle de servidor, porta e URL de HealhCheck.  
As configurações de Timeout das requisições REST utilizando o **RestTemplate** podem ser informadas no **properties da aplicação**  
* **rest.connect-timeout**: Parâmetro que define o timeout da conexão das requisições em segundos. Default: 1s  
* **rest.read-timeout**: Parâmetro que define o timeout da requisição em segundos. Default: 35s  

```java
@Configuration
@ComponentScan(basePackages = { //
		"br.com.arquitetura.sample.spring", //
		"br.com.arquitetura.healthcheck", //
		"br.com.arquitetura.commons" })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@EnableDiscoveryClient
public class Application extends SpringBootServletInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.boot.web.support.SpringBootServletInitializer#
	 * configure(org.springframework.boot.builder.SpringApplicationBuilder)
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return configureApplication(builder);
	}

	/**
	 * Classe de inicialização padrão
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		configureApplication(new SpringApplicationBuilder()).run(args);
	}

	/**
	 * Configuração do modo da aplicação
	 * 
	 * @param builder
	 * @return
	 */
	private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
		Locale.setDefault(new Locale("pt", "BR"));
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
		return builder.sources(Application.class).bannerMode(Banner.Mode.OFF);
	}

	/**
	 * Configuração das propriedades de desserialização JSON do Jackson Provider.
	 * 
	 * @return Jackson2ObjectMapperBuilderCustomizer
	 */
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization() {
		return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
	}

	/**
	 * Parâmetro que define o timeout da conexão com host
	 */
	@Value("${rest.connect-timeout:1}")
	private int restConnectTimeout;

	/**
	 * Parâmetro que define o timeout da requisição
	 */
	@Value("${rest.read-timeout:35}")
	private int restReadTimeout;

	/**
	 * Configuração das propriedades das requisições REST
	 * 
	 * @param restTemplateBuilder
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder //
				.setConnectTimeout(Duration.ofSeconds(restConnectTimeout)) //
				.setReadTimeout(Duration.ofSeconds(restReadTimeout)) //
				.build();
	}

	/**
	 * Configuração dos interceptors para logs de requisição e métricas.
	 * 
	 * @param executionTimeInterceptor
	 * @return
	 */
	@Bean
	public MappedInterceptor mappedInterceptor(HandlerInterceptor executionTimeInterceptor) {
		return new MappedInterceptor(new String[] { "/**" }, executionTimeInterceptor);
	}
}
```  

## Criacao dos profiles
No arquivo `pom.xml` devem ser criados profiles conforme abaixo:
```xml
<profiles>
    <profile>
        <id>loc</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <properties>
            <activatedProperties>loc</activatedProperties>
        </properties>
    </profile>
    <profile>
        <id>dsv</id>
        <properties>
            <activatedProperties>dsv</activatedProperties>
        </properties>
    </profile>
    <profile>
        <id>hml</id>
        <properties>
            <activatedProperties>hml</activatedProperties>
        </properties>
    </profile>
    <profile>
        <id>prd</id>
        <properties>
            <activatedProperties>prd</activatedProperties>
        </properties>
    </profile>
</profiles>
```
Com isso conseguimos injetar propriedades de acordo com o ambiente de compilação da aplicação.  
A principal propriedade será a `<activatedProperties>` que é utilizada no arquivo `application.properties` para indicar qual o profile Spring ativo.  
A escolha do profile é realizada durante a compilação do código no maven `mvn clean package -Pprd`.  
Muito importante especificar a tag `<activeByDefault>` para utilizar um profile padrão quando não é informado durante a compilação. Assim durante testes locais a aplicação será iniciada com esse profile específico.  
## Arquivos de properties  
![properties files](doc/properties-files.png)  
Os arquivos de properties da aplicação devem ser configurados com as nomenclaturas conforme imagem acima. O arquivo de configuração do `Log4J2` deve estar em um arquivo separado.  
![Propriedades](doc/properties-entries.png)  
No arquivo `application.properties`, devem ser declarados todas as propriedades que possuem valor padrão na aplicação ou propriedades que serão informadas em tempo de compilação da aplicação, como é o caso da chave `spring.profiles.active` ou das chaves de senha de banco de dados.  
Serão utilizados parâmetros de substituição `@xxx@` esses parâmetros serão substituídos pelo texto no momento da compilação da aplicação. Podem ser informados durante o build do maven `mvn clean package -Poracle.trb009.user_aas.password=senhaXxx` ou podem ser propriedades já presentes no `pom.xml` como é o caso do `spring.profiles.active` que tem o valor substituído pela tag `<activeByDefault>` informado com uma property do profile como visto anteriormente.  
## Referenciar properties
Para acessar valores das propriedades declaradas nos arquivos de properties, deve ser utilizado a anotação `@Value` do Spring em atributos nas classes desejadas.  
![Value](doc/value-reference.png)  
Para recuperar um valor padrão no caso da propriedade não existir, inserir `":"` separando o nome da propriedade do valor padrão.  
## Swagger-UI
As configurações de exibição do Swagger estão parametrizadas no arquivo de properties da aplicação.  
![Swagger Properties](doc/swagger-properties.png)  
Assim os valores são informados em propriedades do `pom.xml`.
![Swagger POM properties](doc/swagger-pom-properties-top.png)
![Swagger POM properties](doc/swagger-pom-properties-bottom.png)  
Na classe `SwaggerConfig.java` são utilizadas as propriedades para a configuração da interface do Swagger-UI.
```java
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${spring.application.host:}")
	private String host;

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${spring.application.description:}")
	private String applicationDescription;

	@Value("${spring.application.version:}")
	private String applicationVersion;
```

## Eureka
Eureka é um serviço baseado em REST que é utilizado principalmente para registrar as URLs e portas das aplicações.
```properties
#DSH
eureka.client.serviceUrl.defaultZone=http://trberkdsh01.tribanco.com.br:8089/EurekaServerApi/eureka
#PRD
eureka.client.serviceUrl.defaultZone=http://trberkprd01.tribanco.com.br:8089/EurekaServerApi/eureka
```
* **eureka.client.serviceUrl.defaultZone**: Deve ser informado o endereço ao qual a aplicação ira se conectar.
  * DSV e HML: http://trberkdsh01.tribanco.com.br:8089/EurekaServerApi/eureka
  * PRD: http://trberkprd01.tribanco.com.br:8089/EurekaServerApi/eureka 

## Lombok
O lombok é um framework que simplifica a criação de entidades e DTOs reduzindo a quantidade de código na aplicação.  
O Lombok deverá ser instalado na IDE de desenvolvimento. Pode ser seguido o procedimento no [site oficial](https://projectlombok.org/setup/eclipse).  
No site está disponível alguns dos [recursos disponíveis para utilização](https://projectlombok.org/features/all).

## Health Check
Recurso implementado em APIs REST para realizar checagem do ambiente e recursos externos.

```json
{
    "status": false,
    "externalUrl": [
        {
            "url": "https://trbngxhml-int.tribanco.com.br/production/v1/oauth/access-token",
            "status": false,
            "message": "405 - Method Not Allowed"
        }
    ],
    "database": [
        {
            "schema": "jdbc:sybase:Tds:TRBSYBDSV01:4102",
            "status": true,
            "message": "OK"
        },
        {
            "schema": "jdbc:oracle:thin:@bdoramtrd01:50020:TRBD009",
            "status": true,
            "message": "OK"
        },
        {
            "schema": "jdbc:sqlserver://BDSQLPCDD01:1450",
            "status": true,
            "message": "OK"
        }
    ],
    "jvm": {
        "maxMemory": "1799,50MB",
        "totalMemory": "524,50MB",
        "freeMemory": "399,60MB",
        "version": "25.161-b12"
    },
    "java": "1.8.0_161",
    "timeZone": "America/Sao_Paulo",
    "locale": "pt_BR",
    "jacksonDate": "2018-12-23 10:56:20",
    "serverDate": "2018-12-23 10:56:20"
}
```
### Estrutura da exposicao REST
Será exposto um recurso REST `GET - /hc` na aplicação.
* **status:** Validação dos objetos `externalUrl` e `database`. Retona `false` caso ocorra alguma falha de conexão com banco de dados ou URls externas utilizadas pela aplicação.
* **externalUrl:** URLs externas utilizadas pela aplicação para consumir recursos como no exemplo a API de Endereços. É realizada uma requisição GET na URL para validação se o Status está igual a 200.
* **database:** Conexões com o banco de dados. Deve conter uma checagem por instancia acessada. Realiza uma consulta simples em banco para validação da conexão: `SELECT 1`
* **jvm:** Informações da JVM onde a aplicação está em execução.
    * **maxMemory:** Caso não especificado com parâmetro `–Xmx`, será utilizado ¼ da memória total do servidor
    * **totalMemory:** Total de memória consumida pela execução da aplicação, pode variar de acordo com a execução da aplicação. Com a utilização do parâmetro `–server` a execução tende a ser otimizada e o consumo de memória cai consideravelmente. Pode ser especificado com o parâmetro `–Xms`.
    * **freeMemory:** Total de memória livre da JVM, com a chamada do `GC (Garbage Collector)` esse valor aumenta e continua variando conforme execução.
    * **version:** Versão da JVM instalada na máquina.
* **java:** Versão da JRE instalada
* **timeZone:** TimeZone configurada pela aplicação. Importante para validação de fuso horário.
* **locale:** Locale configurada na aplicação. 
* **jacksonDate:** Objeto em formato Date() descerializado pela aplicação utilizando Jackson Provider do Spring.
* **serverDate:** String retornada com o parse de objeto Date() dentro do servidor. Muito importante que os parâmetros `serverDate` e `jacksonDate` estejam iguais. 

### Spring Actuator
Os recursos do framework Spring Actuator serão expostos de acordo com o catalogo no recurso `GET - /actuator` na aplicação.
```json
{
	"_links": {
		"self": {
			"href": "http://localhost:8080/actuator",
			"templated": false
		},
		"health": {
			"href": "http://localhost:8080/actuator/health",
			"templated": false
		},
		"health-component": {
			"href": "http://localhost:8080/actuator/health/{component}",
			"templated": true
		},
		"health-component-instance": {
			"href": "http://localhost:8080/actuator/health/{component}/{instance}",
			"templated": true
		},
		"info": {
			"href": "http://localhost:8080/actuator/info",
			"templated": false
		},
		"metrics": {
			"href": "http://localhost:8080/actuator/metrics",
			"templated": false
		},
		"metrics-requiredMetricName": {
			"href": "http://localhost:8080/actuator/metrics/{requiredMetricName}",
			"templated": true
		}
	}
}
```
São informações da aplicação, métricas e health check utilizado pelo `Eureka`.
O endpoint de `Health Check` do actuator está vinculado ao recurso `GET - /hc` da aplicação. 
### Implementacao
Para a implementação do Health Check será necessário a utilização da biblioteca abaixo:
```xml
<dependency>
	<groupId>br.com.arquitetura</groupId>
	<artifactId>health-check</artifactId>
</dependency>
```
Será necessário realizar a implementação da interface `HealthCheckService`, a qual obriga a implementação do método `healthCheck`.  
Esse método retorna um objeto `HealthCheckDTO` que realiza os testes iniciais de ambiente no seu construtor padrão.  
Um segundo construtor é utilizado para realizar os testes em URLs externas e Datasources conforme exemplo abaixo:
```java
@Service
public class HealthCheckServiceImpl implements HealthCheckService {

	// Datasource Sybase
	@Autowired
	private TipoOperacaoRepository sybaseRepository;

	// Datasource Oracle
	@Autowired
	private StatusRepository oracleTrb009Repository;

	// Datasource SQL Server
	@Autowired
	private CsuTipoPlasticoRepository sqlTrc01Repository;

	// URL de conexão com Sybase
	@Value("${datasource.sybase.prod.url}")
	private String sybaseUrl;

	// URL de conexão com Oracle
	@Value("${datasource.oracle.trb009.url}")
	private String oracleTrb009Url;

	// URL de conexão com SQL Server
	@Value("${datasource.sql.trc01.url}")
	private String sqlTrc01Url;

	// URL Externa utilizada pela aplicação
	@Value("${health.check.api.endereco}")
	private String urlApiEndereco;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.healthcheck.service.HealthCheckService#healthCheck()
	 */
	@Override
	public HealthCheckDTO healthCheck() {
		return new HealthCheckDTO() //
				.withUrls(urlApiEndereco) //
				.withDatabases(new DatabaseDTO(sybaseRepository, sybaseUrl), //
						new DatabaseDTO(oracleTrb009Repository, oracleTrb009Url), //
						new DatabaseDTO(sqlTrc01Repository, sqlTrc01Url));
	}
}
```
O objeto `HealthCheckDTO` oferece os métodos:
* **withUrls:** Recebe uma ou várias URLs que devem ser endpoints que retornem status 200 em um GET realizado a partir da aplicação. Recomendado o uso de Swagger ou WSDL para testes.
* **withDatabases:** Recebe um ou vários objetos `DatabaseDTO` que devem ser preenchidos com o repositório e a URL da conexão.
Na camada de repositórios do banco de dados, deve ser implementado um método com o nome healthCheck de acordo com o banco utilizado:

#### **Oracle**
```java
@Query(nativeQuery = true, value = "SELECT 1 FROM DUAL")
public int healthCheck();
```
#### **SQL Server e Sybase**
```java
@Query(nativeQuery = true, value = "SELECT 1")
public int healthCheck();
```
É necessário realizar a configuração no `Application.java` para possibilitar a inclusão dos recursos da biblioteca no contexto da aplicação.  
Para isso é necessário incluir o pacote base da biblioteca `br.com.arquitetura.healthcheck` na anotação `@ComponentScan`.
```java
@ComponentScan(basePackages = { //
		"br.com.arquitetura.sample.spring", //
		"br.com.arquitetura.healthcheck"})
```
## Tratamento de excecoes
Devido a biblioteca arquitetural `commons`, temos configurado na aplicação um tratamento de exceções para as requisições.
```xml
<dependency>
	<groupId>br.com.arquitetura</groupId>
	<artifactId>commons</artifactId>
</dependency>
```
No código não é necessário realizar tratativas com `try catch` no código da aplicação. Basta deixar a exceção estourar na camada `controller` que ela será interceptada e tratada devidamente pelo `ExceptionHandler` da biblioteca.
```java
// Exemplo de lançamento da exceção
public ConsultaCepResponseDTO consultaCep(String cep) {
	if (!StringUtils.isNumeric(cep)) {
		throw new BusinessException("CEP deve ser numerico!");
	}

	log.debug("Consultando CEP: {}", cep);
	return repository.consultaCep(cep);
}
```
**Retorno**  
![Retorno Exception Handler](doc/exception-handler-return.png)
#### Biblioteca Exception Handler
A biblioteca intercepta automaticamente toda exceção lançada pela aplicação, onde teremos os seguintes cenários:
* **BusinessException:** Exceções de negócio que serão retornadas com status `422 (Unprocessable Entity)` para o cliente. 
* **InfrastructureException:** Exceções de erros internos da aplicação como objetos nulos do banco de dados ou validações para evitar `NullPointerException` por exemplo. Nesses casos o cliente não consegue ajustar na requisição e é retornado um erro `500 (Internal Server Error)` para o cliente.
* **UnauthorizedException:** Exceções de erros relacionados à falhas durante uma tentativa de autenticação, falta de autenticação ou falta de permissão. É retornado um `401 (Unauthorized)` para o cliente.
* **Exception:** Todas as outras exceções que podem ser geradas pela aplicação. Será retornado um erro `500 (Internal Server Error)` para o cliente.

O campo `mensagem` será exibido com a causa raiz da Exceção gerada.

#### Tratamento de exceções no Controller
Não trate exceções no Controller, a não ser em casos específicos e raros (ou seja, **evite blocos try/catch nessa camada**).
O commons faz o papel de tratar as exceções de forma "automática" para a aplicação. A maioria das exceções deveriam retornar 400, 401, 402 ou 500 para o cliente, casos esses que são cobertos pelo próprio Spring e Commons. Caso você queira fazer o tratamento de um tipo de exceção diferente, que muitos de seus controllers precisarão tratar de alguma maneira específica, utilize um [ExceptionHandler](https://www.baeldung.com/exception-handling-for-rest-with-spring) do Spring. Manter o tratamento de exceções centralizado garante a consistência da sua API.
Idealmente os métodos controller terão apenas uma linha de código:

```java
public ResponseEntity<RespostaBaseDTO> metodoController() {
	return ResponseEntity.ok(service.fazerAlgumaCoisa());
}
```

## REST

## Não retorne HTTP 2XX para situações de erro. Trabalhe para que as APIs construídas estejam em conformidade com o [RFC 7231](https://tools.ietf.org/html/rfc7231) e tenham um [nível de maturidade](https://martinfowler.com/articles/richardsonMaturityModel.html) elevado. APIs devem ser fáceis e intuitivas de usar.

### Logging
O projeto está utilizando o [Log4J2](https://logging.apache.org/log4j/2.x/) como framework de log.
#### Implementacao
O `lombok` é utilizado para redução de código nas classes, bastando anotar a classe com `@log4j2`, assim é possível o acesso a constante `log` no código para realizar a gravação de logs.
![Usando Log4J2](doc/log4j2-usage.png)
Importante utilizar devidamente os `níveis de evento` na aplicação. Assim podemos reduzir a quantidade de log conforme necessidade.
![Log Level](doc/log4j2-level-table.png)
#### Configuracao
O arquivo de configuração segue a seguinte estrutura:
* **Properties:** São propriedades utilizadas nas estruturas de appenders e loggers.
    * **Log-path:** Onde o arquivo será gravado na máquina local
    * **Log-name:** Recupera o nome da aplicação do `application.properties` na propriedade `application.name`.
    * **Log-level:** Event Level da aplicação
    * **Log-pattern:** Padrão da mensagem do log gravado no arquivo.
    * **Log-file-pattern:** Padrão do arquivo de log após rotacionar.

        ```json
        "properties": {
            "property": [{
                "name": "log-path",
                "value": "/trb/logsistemas"
            },
            {
                "name": "log-name",
                "value": "${bundle:application:application.name}"
            },
            {
                "name": "log-level",
                "value": "DEBUG"
            },
            {
                "name": "log-pattern",
                "value": "[%-5level] [%X{request-key}] [%t] %d{yyyy-MM-dd HH:mm:ss.SSS} %c{1} - %msg%n"
            },
            {
                "name": "log-file-pattern",
                "value": "%d{yyyy-MM-dd}#%i"
            }]
        }
        ```
* **Appenders:** São estruturas disponíveis para realizar o log da aplicação.
    * **RollingFile:** Grava log em arquivo, rotacionando e compactando ao atingir as restrições das Policies `SizeBasedTriggeringPolicy` que rotaciona o arquivo ao atingir o tamanho de `100MB` no arquivo de log e `TimeBasedTriggeringPolicy` que rotaciona o arquivo por dia.  
	`DefaultRolloverStrategy` que executa a remoção dos arquivos antigos de acordo com as políticas `IfAccumulatedFileSize` que realiza a remoção quando a soma dos arquivos de backup compactados exceder 500MB.
        
        ```json
		"RollingFile": [{
			"name": "Application-Appender",
			"fileName": "${log-path}/${log-name}/${log-name}.log",
			"filePattern": "${log-path}/${log-name}/${log-name}.${log-file-pattern}.log.gz",
			"PatternLayout": {
				"pattern": "${log-pattern}"
			},
			"Policies": {
				"TimeBasedTriggeringPolicy": {
					"interval": "1"
				},
				"SizeBasedTriggeringPolicy": {
					"size": "100 MB"
				}
			},
			"DefaultRolloverStrategy": {
				"compressionLevel": 9,
				"Delete": {
					"basePath": "${log-path}/${log-name}",
					"IfFileName": {
						"glob": "${log-name}.*.log.gz",
						"IfAccumulatedFileSize": {
							"exceeds": "500 MB"
						}
					}
				}
			}
		}
        ```
    * **Gelf:** Envia o log para o servidor do `Graylog`, importante o envio do atributo `facility` com o nome da aplicação e uma propriedade extra `env` com o ambiente de execução da aplicação. Ambos são referenciados as chaves do arquivo `application.properties`.
        ```json
        "Gelf": {
            "name": "Graylog-Appender",
            "host": "udp:graylog.tribanco.com.br",
            "port": "8100",
            "version": "1.1",
            "extractStackTrace": "true",
            "filterStackTrace": "true",
            "facility": "${bundle:application:application.name}",
            "mdcProfiling": "true",
            "includeFullMdc": "true",
            "maximumMessageSize": "8192",
            "originHost": "%host",
            "Field": [{
				"name": "className",
				"pattern": "%C"
			},
			{
				"name": "line",
				"pattern": "%L"
			},
			{
				"name": "method",
				"pattern": "%M"
			},
			{
				"name": "log-level",
				"pattern": "%-5level"
			},
			{
				"name": "thread",
				"pattern": "%t"
			},
			{
				"name": "thread-id",
				"pattern": "%tid"
			},
			{
				"name": "env",
				"pattern": "${bundle:application:spring.profiles.active}"
			}]
        }
        ```
* **Loggers:** São as definições de quais recursos serão enviados para quais appenders.
    * Obrigatório um logger para o pacote base da aplicação.
    * Obrigatório um logger para o pacote base da biblioteca `commons (br.com.arquitetura.commons)`
* **Root Logger:** É a configuração de logging de pacotes que não foram declarados nos `loggers`, apontado para um arquivo separado com pós fixo `console`.
    ```json
    "loggers": {
        "logger": [{
            "name": "br.com.arquitetura.sample.spring",
            "level": "${log-level}",
            "additivity": "false",
            "appender-ref": [{
                "ref": "Graylog-Appender"
            },
            {
                "ref": "Application-Appender"
            }]
        },
        {
            "name": "br.com.arquitetura.commons",
            "level": "${log-level}",
            "additivity": "false",
            "appender-ref": [{
                "ref": "Graylog-Appender"
            },
            {
                "ref": "Application-Appender"
            }]
        }],
        "root": {
            "level": "INFO",
            "appender-ref": [{
                "ref": "Console-Appender"
            }]
        }
    }
    ```
### Testes unitarios
![Testes unitarios](doc/unit-test-package.png)  
Toda a camada de negócio da aplicação `service` deverá ter testes unitários para garantir as regras implementadas na aplicação.  
Deverá conter um `log4j2.json` para apontar todo o log para o console durante os testes unitários.  
Na aplicação são utilizados os frameworks de teste: 
* **JUnit:** Executa os testes unitários.  
* **Jacoco:** Gera o relatório de cobertura de testes da aplicação.  
* **Mockito:** Realiza mocks dos repositórios que são utilizados na classe para isolar as demais camadas. Assim será testado apenas a camada service.

```java
public class EnderecoServiceTest {

	@InjectMocks
	private EnderecoService service;

	@Mock
	private EnderecoApiRepository repository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void consultaCep() {
		Mockito.when(repository.consultaCep(Mockito.anyString())).thenReturn(new ConsultaCepResponseDTO());
		Assert.notNull(service.consultaCep("38400000"), "Retorno do consulta CEP nao deve ser NULL");
	}

	@Test(expected = BusinessException.class)
	public void consultaCep_error() {
		service.consultaCep("abc");
	}
}
```
### Acesso a base de dados
A camada de acesso a base de dados deverá ser implementada utilizando repositórios do Spring Data JPA.  
**DE FORMA ALGUMA DEVERÁ SER REALIZADO O ACESSO DIRETO AO ENTITY MANAGER DA APLICAÇÃO**.  
#### Data Source
A configuração do data source é realizada por classes de configuração no pacote `br.com.arquitetura.sample.spring.config.datasource`.
Utilizamos o **Hikari** já contido no **Spring Boot 2.x** para o gerenciamento de conexão da aplicação.
As classes de mapeamento de entidades e repositórios de acesso a base de dados devem estar em pacotes organizados por **data source** e devem ser informados na classe de configuração.  
![Configuracao Sybase](doc/sybase-conf.png)  
![Configuracao Oracle](doc/oracle-conf.png)  
![Configuracao SQL](doc/sql-conf.png)  
#### Pool de Conexao
Por padrão, a aplicação mantém 10 conexões abertas com o banco de dados para utilização do pool, podendo chegar a 15 conexões com um timeout de 10 minutos de inatividade.  
Os valores podem ser ajustados conforme necessidade da aplicação com as propriedades `minimumIdle` e `maximumPoolSize` do datasource.  
As configurações são realizadas no arquivo de properties e são definidas para cada datasource configurado.  
Ao realizar o mapeamento de entidades do banco de dados em classes, podemos simplesmente criar um repositório com os recursos de CRUD prontos do Framework.  
![Estrutura DB](doc/estrutura-db.png)  
```java
@Data
@Entity
@Table(name = "TBL_TIPO_OPERACAO", schema = "dbo", catalog = "db_npc")
public class TipoOperacaoEntity {

	@Id
	@Column(name = "CD_TIPO_OPERACAO")
	private Long cdTipoOperacao;

	@Column(name = "DS_TIPO_OPERACAO")
	private String dsTipoOperacao;

}
```
Na entidade serão declarados os atributos e especificado através de anotações o nome da tabela, colunas e banco de dados para conexão e geração das queries dinâmicas no JPA.
```java
public interface TipoOperacaoRepository extends CrudRepository<TipoOperacaoEntity, Long> {
	
	@Query(nativeQuery = true, value = "SELECT 1")
	public int healthCheck();
}
```
Com a implementação do repositório, podemos inserir queries ou utilizar os recursos disponíveis com a implementação do [CrudRepository](https://docs.spring.io/spring-data/jpa/docs/current/reference/html).  
![Crud Repository](doc/crud-repository.png)  
#### Queries nativas
Existe também a necessidade de trabalhar com `queries nativas` utilizando o Spring Data JPA.  
Assim deve ser criado uma entidade genérica para conter as queries, as queries devem ser distribuídas em entidades com alguma organização lógica para manter a organização do código.  
![Query Nativa](doc/estrutura-query-nativa.png)  
As queries devem ser construídas como `NamedNativeQueries`, em uma classe anotada como `@Entity` que deve possuir apenas o `id` como atributo. O atributo `resultClass` pode ser uma tabela já mapeada ou um DTO criado e anotado como `@Entity`.  
```java
@NamedNativeQueries({ //
	@NamedNativeQuery(name = "Trc01GenericEntity.exampleQuery", //
	query = "SELECT " + //
			"	*   " + //
			"	FROM  TBL_CSU_TIPO_PLASTICO  " + //
			"	WHERE CD_ID_TIPO_PLASTICO = :cdIdTipoPlastico", //
	resultClass = CsuTipoPlasticoEntity.class)
})

@Entity
@Data
public class Trc01GenericEntity implements Serializable{
	 
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;

}
```
No repositório devemos referenciar a Query e os parâmetros de acordo com o exemplo abaixo:
```java
@Repository
public interface Trc01GenericRepository extends CrudRepository<Trc01GenericEntity, Long>{
	
	@Query(nativeQuery = true)
	public CsuTipoPlasticoEntity exampleQuery(@Param("cdIdTipoPlastico")Long cdIdTipoPlastico);
}
```
#### Stored Procedures
Para a execução de procedures em `SQL Server` ou `Sybase`, deverá ser realizado o mapeamento como `Query Nativa` conforme exemplo anterior.  
Para procedures no `Oracle`, temos uma estrutura parecida com as queries nativas, porém utilizando `@NamedStoredProcedureQueries` para configuração da chamada.
```java
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name = "desbloquearConta", procedureName = "SDBANCO.SCCAPIDESBLOQUEARCC", parameters = {
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PAGENCIA", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PNUMCONTA", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PMOTBLOQUEIOCC", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.OUT, name = "PERRO", type = Long.class) })
})

@Entity
@Data
public class Trb009GenericEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
}
```
E da mesma forma que as queries, deve ser referenciado no respectivo repositório.
```java
@Repository
public interface Trb009GenericRepository extends CrudRepository<Trb009GenericEntity, Long>{
	
	@Procedure(name = "bloquearConta")
	Long desbloquearContaCorrente(@Param("PAGENCIA") Long pAgencia, @Param("PNUMCONTA") Long pNumConta,
			@Param("PMOTBLOQUEIOCC") Long pMotBloqueioCC);
}
```