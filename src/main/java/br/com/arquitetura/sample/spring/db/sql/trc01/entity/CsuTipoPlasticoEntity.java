package br.com.arquitetura.sample.spring.db.sql.trc01.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TBL_CSU_TIPO_PLASTICO", schema = "dbo", catalog = "DB_PROCESSADORA")
public class CsuTipoPlasticoEntity {

	@Id
	@Column(name = "CD_ID_TIPO_PLASTICO")
	private Long cdIdTipoPlastico;

	@Column(name = "CD_TIPO_PLASTICO_INICIO")
	private String cdTipoPlasticoInicio;

	@Column(name = "CD_TIPO_PLASTICO_FIM")
	private String cdTipoPlasticoFim;

	@Column(name = "DS_TIPO_PLASTICO")
	private String dsTipoPlastico;
}
