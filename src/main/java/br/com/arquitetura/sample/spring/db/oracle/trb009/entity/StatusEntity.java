package br.com.arquitetura.sample.spring.db.oracle.trb009.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TBL_STATUS", schema = "USER_AAS")
public class StatusEntity {

	@Id
	@Column(name = "CD_STATUS")
	private Long cdStatus;

	@Column(name = "DS_STATUS")
	private String dsStatus;

}
