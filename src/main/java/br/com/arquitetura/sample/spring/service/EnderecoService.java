package br.com.arquitetura.sample.spring.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.arquitetura.commons.exception.BusinessException;
import br.com.arquitetura.sample.spring.external.dto.ConsultaCepResponseDTO;
import br.com.arquitetura.sample.spring.external.repository.EnderecoApiRepository;
import lombok.extern.log4j.Log4j2;

/**
 * Descrição do serviço
 *
 */
@Log4j2
@Service
public class EnderecoService {

	@Autowired
	private EnderecoApiRepository repository;

	@Autowired
	public EnderecoService() {
		// Construtor padrao
	}

	public ConsultaCepResponseDTO consultaCep(String cep) {
		if (!StringUtils.isNumeric(cep)) {
			throw new BusinessException("CEP deve ser numerico!");
		}

		log.debug("Consultando CEP: {}", cep);
		return repository.consultaCep(cep);
	}

}
