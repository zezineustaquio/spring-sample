package br.com.arquitetura.sample.spring.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Configuração do Datasource Sybase para instancia PROD
 * 
 */
@Profile("!test")
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "datasource.sybase.prod")
@EnableJpaRepositories(//
		basePackages = "br.com.arquitetura.sample.spring.db.sybase.repository", //
		entityManagerFactoryRef = "sybase-prod-em", //
		transactionManagerRef = "sybase-prod-tm")
public class SybaseProdConfig extends HikariConfig {

	/**
	 * Factory para criação do Data Source
	 * 
	 * @return
	 */
	@Bean(name = "sybase-prod-ds")
	@Primary
	public DataSource sybaseProdDataSourceFactory() {
		return new HikariDataSource(this);
	}

	/**
	 * Factory para criação do Entity Manager
	 * 
	 * @param builder
	 * @return
	 */
	@Primary
	@PersistenceContext(unitName = "SYBASE")
	@Bean(name = "sybase-prod-em")
	public LocalContainerEntityManagerFactoryBean sybaseProdEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(sybaseProdDataSourceFactory()).persistenceUnit("SYBASE").properties(jpaProperties())
				.packages("br.com.arquitetura.sample.spring.db.sybase.entity").build();
	}

	/**
	 * Factory para criação do Transaction Manager
	 * 
	 * @param em
	 * @return
	 */
	@Primary
	@Bean(name = "sybase-prod-tm")
	public PlatformTransactionManager sybaseTransactionManagerFactory(
			@Qualifier("sybase-prod-em") EntityManagerFactory em) {
		return new JpaTransactionManager(em);
	}

	/**
	 * Propriedades do Persistence Unity
	 * 
	 * @return
	 */
	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.dialect", "org.hibernate.dialect.SybaseASE157Dialect");
		return props;
	}
}
