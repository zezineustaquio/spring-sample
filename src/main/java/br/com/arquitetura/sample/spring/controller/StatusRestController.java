package br.com.arquitetura.sample.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.arquitetura.sample.spring.db.oracle.trb009.entity.StatusEntity;
import br.com.arquitetura.sample.spring.service.StatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Documentação do Controller
 * 
 */
@RestController
@RequestMapping("/status")
@Api(tags = { "API de Status" })
public class StatusRestController {
	
	/**
	 * Funcao basica do serviço
	 */
	@Autowired
	private StatusService service;

	/**
	 * Construtor padrão
	 */
	@Autowired
	public StatusRestController() {
		// Construtor padrão
	}

	/**
	 * Documentacao do metodo
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Busca todos os Status")
	public ResponseEntity<List<StatusEntity>> findAllStatus() {
		return new ResponseEntity<>(service.findAllStatus(), HttpStatus.OK);
	}

}
