package br.com.arquitetura.sample.spring.db.oracle.trb009.repository;

import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.oracle.trb009.entity.Trb009GenericEntity;

@Repository
public interface Trb009GenericRepository extends CrudRepository<Trb009GenericEntity, Long>{
	
	@Procedure(name = "bloquearConta")
	Long desbloquearContaCorrente(@Param("PAGENCIA") Long pAgencia, @Param("PNUMCONTA") Long pNumConta,
			@Param("PMOTBLOQUEIOCC") Long pMotBloqueioCC);
}