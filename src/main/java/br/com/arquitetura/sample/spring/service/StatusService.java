package br.com.arquitetura.sample.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.arquitetura.sample.spring.db.oracle.trb009.entity.StatusEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb009.repository.StatusRepository;
import lombok.extern.log4j.Log4j2;

/**
 * Descrição do serviço
 * 
 */
@Log4j2
@Service
public class StatusService {

	/**
	 * Funcao basica do repositorio
	 */
	@Autowired
	private StatusRepository statusRepository;

	/**
	 * Construtor padrão
	 */
	@Autowired
	public StatusService() {
		// Construtor padrão
	}

	/**
	 * Documentacao do metodo
	 * 
	 * @return
	 */
	public List<StatusEntity> findAllStatus() {
		log.debug("Listando Status.");
		return (List<StatusEntity>) statusRepository.findAll();
	}

}
