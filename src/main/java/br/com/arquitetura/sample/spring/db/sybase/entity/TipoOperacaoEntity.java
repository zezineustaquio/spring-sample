package br.com.arquitetura.sample.spring.db.sybase.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TBL_TIPO_OPERACAO", schema = "dbo", catalog = "db_npc")
public class TipoOperacaoEntity {

	@Id
	@Column(name = "CD_TIPO_OPERACAO")
	private Long cdTipoOperacao;

	@Column(name = "DS_TIPO_OPERACAO")
	private String dsTipoOperacao;

}
