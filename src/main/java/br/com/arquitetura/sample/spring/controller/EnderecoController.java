package br.com.arquitetura.sample.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.arquitetura.sample.spring.external.dto.ConsultaCepResponseDTO;
import br.com.arquitetura.sample.spring.service.EnderecoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Documentação do Controller
 */
@RestController
@RequestMapping("/endereco")
@Api(tags = { "API de Endereco" })
public class EnderecoController {

	@Autowired
	private EnderecoService service;

	@Autowired
	public EnderecoController() {
		// Construtor padrao
	}

	/**
	 * Documentacao do metodo
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{cep}")
	@ApiOperation(value = "Recupera o endereço referente ao CEP passado")
	public ResponseEntity<ConsultaCepResponseDTO> findByCep(
			@ApiParam(value = "CEP do endereco, pode ser com ou sem máscara.") @PathVariable String cep) {
		return new ResponseEntity<>(service.consultaCep(cep), HttpStatus.OK);
	}

}
