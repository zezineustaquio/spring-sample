package br.com.arquitetura.sample.spring.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Configuração do Datasource SQL Server para instancia TRC01
 * 
 */
@Profile("!test")
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "datasource.sql.trc01")
@EnableJpaRepositories(//
		basePackages = "br.com.arquitetura.sample.spring.db.sql.trc01.repository", //
		entityManagerFactoryRef = "sql-trc01-em", //
		transactionManagerRef = "sql-trc01-tm")
public class SqlTrc01Config extends HikariConfig {

	/**
	 * Factory para criação do Data Source
	 * 
	 * @return
	 */
	@Bean(name = "sql-trc01-ds")
	public DataSource oracleTrb009DataSourceFactory() {
		return new HikariDataSource(this);
	}

	/**
	 * Factory para criação do Entity Manager
	 * 
	 * @param builder
	 * @return
	 */
	@PersistenceContext(unitName = "TRC01")
	@Bean(name = "sql-trc01-em")
	public LocalContainerEntityManagerFactoryBean oracleTrb009EntityManagerFactory(
			EntityManagerFactoryBuilder builder) {
		return builder.dataSource(oracleTrb009DataSourceFactory()).persistenceUnit("TRC01").properties(jpaProperties())
				.packages("br.com.arquitetura.sample.spring.db.sql.trc01.entity").build();
	}

	/**
	 * Factory para criação do Transaction Manager
	 * 
	 * @param em
	 * @return
	 */
	@Bean(name = "sql-trc01-tm")
	public PlatformTransactionManager oracleTrb009TransactionManagerFactory(
			@Qualifier("sql-trc01-em") EntityManagerFactory em) {
		return new JpaTransactionManager(em);
	}

	/**
	 * Propriedades do Persistence Unity
	 * 
	 * @return
	 */
	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2005Dialect");
		return props;
	}
}
