package br.com.arquitetura.sample.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.arquitetura.healthcheck.DTO.DatabaseDTO;
import br.com.arquitetura.healthcheck.DTO.HealthCheckDTO;
import br.com.arquitetura.healthcheck.service.HealthCheckService;
import br.com.arquitetura.sample.spring.db.oracle.trb009.repository.StatusRepository;
import br.com.arquitetura.sample.spring.db.sql.trc01.repository.CsuTipoPlasticoRepository;
import br.com.arquitetura.sample.spring.db.sybase.repository.TipoOperacaoRepository;

/**
 * Implementação do recurso de Health Check Deve ser implementado para monitorar
 * o acesso a URLs externas e repositórios de banco de dados utilizando Spring
 * Data JPA
 */
@Service
public class HealthCheckServiceImpl implements HealthCheckService {

	@Autowired
	private TipoOperacaoRepository sybaseRepository;

	@Autowired
	private StatusRepository oracleTrb009Repository;

	@Autowired
	private CsuTipoPlasticoRepository sqlTrc01Repository;

	@Value("${datasource.sybase.prod.jdbcUrl}")
	private String sybaseUrl;

	@Value("${datasource.oracle.trb009.jdbcUrl}")
	private String oracleTrb009Url;

	@Value("${datasource.sql.trc01.jdbcUrl}")
	private String sqlTrc01Url;

	@Value("${health.check.api.endereco}")
	private String urlApiEndereco;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.arquitetura.healthcheck.service.HealthCheckService#healthCheck()
	 */
	@Override
	public HealthCheckDTO healthCheck() {
		return new HealthCheckDTO() //
				.withUrls(urlApiEndereco) //
				.withDatabases(new DatabaseDTO(sybaseRepository, sybaseUrl), //
						new DatabaseDTO(oracleTrb009Repository, oracleTrb009Url), //
						new DatabaseDTO(sqlTrc01Repository, sqlTrc01Url));
	}

}
