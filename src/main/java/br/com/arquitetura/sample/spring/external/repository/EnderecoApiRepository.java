package br.com.arquitetura.sample.spring.external.repository;

import br.com.arquitetura.sample.spring.external.dto.ConsultaCepResponseDTO;

@FunctionalInterface
public interface EnderecoApiRepository {

	ConsultaCepResponseDTO consultaCep(String cep);

}
