package br.com.arquitetura.sample.spring.db.sql.trc01.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.arquitetura.sample.spring.db.sql.trc01.entity.CsuTipoPlasticoEntity;

/**
 * Descrição do repositorio
 * 
 */
public interface CsuTipoPlasticoRepository extends CrudRepository<CsuTipoPlasticoEntity, Long> {

	@Query(nativeQuery = true, value = "SELECT 1")
	public int healthCheck();
}
