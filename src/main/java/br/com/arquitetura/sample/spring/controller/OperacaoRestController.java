package br.com.arquitetura.sample.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.arquitetura.sample.spring.db.sql.trc01.entity.CsuTipoPlasticoEntity;
import br.com.arquitetura.sample.spring.db.sybase.entity.TipoOperacaoEntity;
import br.com.arquitetura.sample.spring.service.OperacaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Documentação do Controller
 * 
 */
@RestController
@RequestMapping("/operacao")
@Api(tags = { "API de Operação" })
public class OperacaoRestController {
	
	/**
	 * Funcao basica do serviço
	 */
	@Autowired
	private OperacaoService service;

	/**
	 * Construtor padrão
	 */
	@Autowired
	public OperacaoRestController() {
		// Construtor padrão
	}

	/**
	 * Documentacao do metodo
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation(value = "Busca todos os Tipos de Operação")
	public ResponseEntity<List<TipoOperacaoEntity>> findAllTipos() {
		return new ResponseEntity<>(service.findAllTipos(), HttpStatus.OK);
	}
	@RequestMapping(method = RequestMethod.GET, value = "/{cdIdTipoPlastico}")
	@ApiOperation(value = "Busca o tipo plastico informado")
	public ResponseEntity<CsuTipoPlasticoEntity> exampleQuery(@PathVariable Long cdIdTipoPlastico) {
		return new ResponseEntity<>(service.exampleQuery(cdIdTipoPlastico), HttpStatus.OK);
	}
}
