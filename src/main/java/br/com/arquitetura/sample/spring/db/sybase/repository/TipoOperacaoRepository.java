package br.com.arquitetura.sample.spring.db.sybase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.arquitetura.sample.spring.db.sybase.entity.TipoOperacaoEntity;

/**
 * Descrição do repositorio
 * 
 */
public interface TipoOperacaoRepository extends CrudRepository<TipoOperacaoEntity, Long> {
	
	@Query(nativeQuery = true, value = "SELECT 1")
	public int healthCheck();
}
