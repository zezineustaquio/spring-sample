package br.com.arquitetura.sample.spring.db.oracle.trb009.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.oracle.trb009.entity.StatusEntity;

/**
 * Descrição do repositorio
 * 
 */
@Repository
public interface StatusRepository extends CrudRepository<StatusEntity, Long> {
	
	@Query(nativeQuery = true, value = "SELECT 1 FROM DUAL")
	public int healthCheck();
}
