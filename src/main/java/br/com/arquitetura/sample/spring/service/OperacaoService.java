package br.com.arquitetura.sample.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.arquitetura.sample.spring.db.sql.trc01.entity.CsuTipoPlasticoEntity;
import br.com.arquitetura.sample.spring.db.sql.trc01.repository.Trc01GenericRepository;
import br.com.arquitetura.sample.spring.db.sybase.entity.TipoOperacaoEntity;
import br.com.arquitetura.sample.spring.db.sybase.repository.TipoOperacaoRepository;
import lombok.extern.log4j.Log4j2;

/**
 * Descrição do serviço
 * 
 */
@Log4j2
@Service
public class OperacaoService {

	/**
	 * Funcao basica do repositorio
	 */
	@Autowired
	private TipoOperacaoRepository tipoOperacaoRepository;
	
	@Autowired
	private Trc01GenericRepository trc01GenericRepository;

	/**
	 * Construtor padrão
	 */
	@Autowired
	public OperacaoService() {
		// Construtor padrão
	}

	/**
	 * Documentacao do metodo
	 * 
	 * @return
	 */
	public List<TipoOperacaoEntity> findAllTipos() {
		log.debug("Listando Operacoes.");
		return (List<TipoOperacaoEntity>) tipoOperacaoRepository.findAll();
	}
	public CsuTipoPlasticoEntity exampleQuery(Long cdIdTipoPlastico) {
		return trc01GenericRepository.exampleQuery(cdIdTipoPlastico);
	}

}
