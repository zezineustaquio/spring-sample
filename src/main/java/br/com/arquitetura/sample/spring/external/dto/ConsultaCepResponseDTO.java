package br.com.arquitetura.sample.spring.external.dto;

import lombok.Data;

@Data
public class ConsultaCepResponseDTO {
	private String logradouro;
	private String localidade;
	private String bairro;
	private String uf;
	private String cep;
}
