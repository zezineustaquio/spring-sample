package br.com.arquitetura.sample.spring.db.oracle.trb009.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import lombok.Data;


/**
 * Este exemplo pode ser adotado em chamadas de procedures que tenham parametro de saida (OUT) 
 * 
 */
@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name = "desbloquearConta", procedureName = "SDBANCO.SCCAPIDESBLOQUEARCC", parameters = {
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PAGENCIA", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PNUMCONTA", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.IN, name = "PMOTBLOQUEIOCC", type = Long.class),
			@StoredProcedureParameter(mode = ParameterMode.OUT, name = "PERRO", type = Long.class) })
})

@Entity
@Data
public class Trb009GenericEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;
}