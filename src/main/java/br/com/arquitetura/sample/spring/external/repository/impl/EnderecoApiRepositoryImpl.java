package br.com.arquitetura.sample.spring.external.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.commons.api.RequestUtil;
import br.com.arquitetura.commons.api.dto.OauthDTO;
import br.com.arquitetura.sample.spring.external.dto.ConsultaCepResponseDTO;
import br.com.arquitetura.sample.spring.external.repository.EnderecoApiRepository;

@Repository
public class EnderecoApiRepositoryImpl implements EnderecoApiRepository {

	@Autowired
	private RequestUtil rest;

	@Value("${api.oauth.client.id}")
	private String clientId;

	@Value("${api.oauth.client.secret}")
	private String clientSecret;

	@Value("${api.oauth.url}")
	private String oauthUrl;

	@Value("${api.endereco.cep.abreviado.url}")
	private String consultaCepUrl;

	@Override
	public ConsultaCepResponseDTO consultaCep(String cep) {

		return rest.sendGatewayRequest( //
				String.format(consultaCepUrl, cep), //
				HttpMethod.GET, //
				new ParameterizedTypeReference<ConsultaCepResponseDTO>() {
				}, //
				new OauthDTO(clientId, clientSecret, oauthUrl));
	}

}