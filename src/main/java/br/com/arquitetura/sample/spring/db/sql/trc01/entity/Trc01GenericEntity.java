package br.com.arquitetura.sample.spring.db.sql.trc01.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;

import lombok.Data;

/**
 * @param name Deve conter o nome da entitade generica seguindo de ponto e o nome do recurso no repositorio generico
 * @param query Deve conter a query nativa a ser executada 
 * @param resultClass Deve conter a entity onde o resultado da consulta deve ser mapeado
 */
@NamedNativeQueries({ //
	@NamedNativeQuery(name = "Trc01GenericEntity.exampleQuery", //
	query = "SELECT " + //
			"	*   " + //
			"	FROM  TBL_CSU_TIPO_PLASTICO  " + //
			"	WHERE CD_ID_TIPO_PLASTICO = :cdIdTipoPlastico", //
	resultClass = CsuTipoPlasticoEntity.class)
})

@Entity
@Data
public class Trc01GenericEntity implements Serializable{
	 
	private static final long serialVersionUID = 1L;
	
	@Id
	private Long id;

}
