package br.com.arquitetura.sample.spring.db.sql.trc01.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.sql.trc01.entity.CsuTipoPlasticoEntity;
import br.com.arquitetura.sample.spring.db.sql.trc01.entity.Trc01GenericEntity;

@Repository
public interface Trc01GenericRepository extends CrudRepository<Trc01GenericEntity, Long>{
	
	@Query(nativeQuery = true)
	public CsuTipoPlasticoEntity exampleQuery(@Param("cdIdTipoPlastico")Long cdIdTipoPlastico);
}
