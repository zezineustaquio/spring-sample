package br.com.arquitetura.sample.spring.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;

import br.com.arquitetura.commons.exception.BusinessException;
import br.com.arquitetura.sample.spring.external.dto.ConsultaCepResponseDTO;
import br.com.arquitetura.sample.spring.external.repository.EnderecoApiRepository;
import br.com.arquitetura.sample.spring.service.EnderecoService;

public class EnderecoServiceTest {

	@InjectMocks
	private EnderecoService service;

	@Mock
	private EnderecoApiRepository repository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void consultaCep() {
		Mockito.when(repository.consultaCep(Mockito.anyString())).thenReturn(new ConsultaCepResponseDTO());
		Assert.notNull(service.consultaCep("38400000"), "Retorno do consulta CEP nao deve ser NULL");
	}

	@Test(expected = BusinessException.class)
	public void consultaCep_error() {
		service.consultaCep("abc");
	}

}
