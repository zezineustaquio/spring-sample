package br.com.arquitetura.sample.spring.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.arquitetura.sample.spring.db.sql.trc01.entity.CsuTipoPlasticoEntity;
import br.com.arquitetura.sample.spring.db.sql.trc01.repository.Trc01GenericRepository;
import br.com.arquitetura.sample.spring.db.sybase.repository.TipoOperacaoRepository;
import br.com.arquitetura.sample.spring.service.OperacaoService;

public class OperacaoServiceTest {

	@InjectMocks
	private OperacaoService service;

	@Mock
	private TipoOperacaoRepository tipoOperacaoRepository;

	@Mock
	private Trc01GenericRepository trc01GenericRepository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findAllTipos() {
		Mockito.when(tipoOperacaoRepository.findAll()).thenReturn(null);
		service.findAllTipos();
	}

	@Test
	public void exampleQuery() {
		Mockito.when(trc01GenericRepository.exampleQuery(Mockito.anyLong())).thenReturn(new CsuTipoPlasticoEntity());
		service.exampleQuery(1L);
	}

}
