package br.com.arquitetura.sample.spring.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.arquitetura.sample.spring.db.oracle.trb009.repository.StatusRepository;
import br.com.arquitetura.sample.spring.service.StatusService;

public class StatusServiceTest {

	@InjectMocks
	private StatusService service;

	@Mock
	private StatusRepository statusRepository;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void findAllStatus() {
		Mockito.when(statusRepository.findAll()).thenReturn(null);
		service.findAllStatus();
	}

}
